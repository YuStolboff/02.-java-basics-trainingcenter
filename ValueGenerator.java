package com.epam.training.javaBasics.stolbovy;

import java.util.Random;

class ValueGenerator {

    int generatesNaturalNumber(String dataType) {
        int number = 0;
        Random random = new Random();
        switch (dataType) {
            case "mark":
                number = 3 + random.nextInt(3);
                break;
            case "duration":
                number = 8 + random.nextInt(24);
                break;
            case "startDateReceiptOfTask":
                number = 8 + random.nextInt(72);
                break;
        }
        return number;
    }

    String generatesNameOfCurriculum() {
        String curriculum = "";
        Random random = new Random();
        int number = random.nextInt(2);
        switch (number) {
            case 0:
                curriculum = "Java Developer";
                break;
            case 1:
                curriculum = "J2EE Developer";
                break;
        }
        return curriculum;
    }
}
