package com.epam.training.javaBasics.stolbovy;

import java.util.ArrayList;
import java.util.List;

import static com.epam.training.javaBasics.stolbovy.Database.students;
import static java.lang.Math.round;

class WorkOnDatabaseOfStudents {

    private final static double PASSING_MARK = 4.5;
    private final static int MAX_MARK = 5;
    private final static int NUMBER_OF_HOURS = 8;

    int calculatesHoursBeforeEndOfProgram(String nameOfStudent) {
        Student student = findsStudentNamed(nameOfStudent);
        int durationOfCurriculum = 0;
        int hoursBeforeEndOfProgram;
        Curriculum curriculum = student.getCurriculum();
        for (Course course : curriculum.getCourse()) {
            durationOfCurriculum += course.getDuration();
        }
        hoursBeforeEndOfProgram = durationOfCurriculum - student.getStartDateReceiptOfTask();
        if (hoursBeforeEndOfProgram < 0) {
            return 0;
        } else {
            return hoursBeforeEndOfProgram;
        }
    }

    double calculatesCurrentAverageMark(String nameOfStudent) {
        Student student = findsStudentNamed(nameOfStudent);
        if (student.getMark().isEmpty()) {
            return 0;
        }
        double sumOfMarks = 0;
        int numberOfMarks = 0;
        for (Integer mark : student.getMark()) {
            sumOfMarks += mark;
            numberOfMarks++;
        }
        return sumOfMarks / numberOfMarks;
    }

    boolean calculatePossibilityOfNotDeductingStudent(String nameOfStudent) {
        Student student = findsStudentNamed(nameOfStudent);
        if (student.getMark().isEmpty()) {
            return true;
        }
        double sumOfMarks = 0;
        int numberOfMarks = 0;
        int numberOfProbableMarks;
        double maximumPossibleMark;
        for (Integer mark : student.getMark()) {
            sumOfMarks += mark;
            numberOfMarks++;
        }
        numberOfProbableMarks = (student.getStartDateReceiptOfTask() - numberOfMarks * NUMBER_OF_HOURS);
        maximumPossibleMark = (sumOfMarks + numberOfProbableMarks * MAX_MARK) / (numberOfMarks + numberOfProbableMarks);
        return round(maximumPossibleMark * 10d) / 10d >= PASSING_MARK;
    }

    private Student findsStudentNamed(String nameOfStudent) {
        String[] name = nameOfStudent.split(" ");
        String lastName = name[0];
        String firstName = name[1];
        String middleName;
        if (name.length < 3) {
            middleName = "";
        } else {
            middleName = name[2];
        }
        List<Student> listStudents = students;
        for (Student student : listStudents) {
            if (student.getLastName().toLowerCase().equals(lastName.toLowerCase()) && student.getFirstName()
                    .toLowerCase().equals(firstName.toLowerCase()) && student.getMiddleName().toLowerCase()
                    .equals(middleName.toLowerCase())) {
                return student;
            }
        }
        System.out.println("Такого студента не существует или неправильно заданы параметры поиска.");
        return new Student("", "", "", new Curriculum("", new ArrayList<>()), 0, new ArrayList<>());
    }
}
