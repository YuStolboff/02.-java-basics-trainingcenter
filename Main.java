package com.epam.training.javaBasics.stolbovy;

import static com.epam.training.javaBasics.stolbovy.Database.students;

public class Main {

    public static void main(String[] args) {
        OutputOfResults outputOfResults = new OutputOfResults();
        System.out.println("Список всех студентов:");
        System.out.println();
        System.out.println(outputOfResults.printsStudentsList(students));
        System.out.println();
        System.out.println("Список студентов, отсортированных по среднему баллу:");
        System.out.println();
        System.out.println(outputOfResults.printsStudentsList(outputOfResults.sortsListOfAverageMark(students)));
        System.out.println();
        System.out.println("Список студентов, отсортированных по времени до окончания обучения::");
        System.out.println();
        System.out
                .println(outputOfResults.printsStudentsList(outputOfResults.sortsListOfTimeUntilGraduation(students)));
        System.out.println();
        System.out.println("Список студентов, отфильтрованных по возможности продолжить обучение:");
        System.out.println();
        System.out.println(
                outputOfResults.printsStudentsList(outputOfResults.filtersListOfProbabilityTraining(students)));
    }
}
