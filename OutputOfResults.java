package com.epam.training.javaBasics.stolbovy;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.round;

class OutputOfResults {

    private String printsStudent(Student student) {
        StringBuilder stringBuilder = new StringBuilder();
        String nameOfStudents = student.getLastName() + " " + student.getFirstName() + " " + student.getMiddleName();
        WorkOnDatabaseOfStudents workOnDatabaseOfStudents = new WorkOnDatabaseOfStudents();
        int hoursBeforeEndOfProgram = workOnDatabaseOfStudents.calculatesHoursBeforeEndOfProgram(nameOfStudents);
        boolean deduction = workOnDatabaseOfStudents.calculatePossibilityOfNotDeductingStudent(nameOfStudents);
        double currentAverageMark = workOnDatabaseOfStudents.calculatesCurrentAverageMark(nameOfStudents);
        String result;
        if (!deduction) {
            result = " \r\nОтчислить.";
        } else {
            result = "\r\nМожет продолжать обучение.";
        }
        stringBuilder = stringBuilder.append(nameOfStudents).append(" - До окончания обучения по программе ")
                .append(student.getCurriculum().getNameOfCurriculum()).append(" осталось ")
                .append(hoursBeforeEndOfProgram).append("ч. ").append("Средний балл ")
                .append(round(currentAverageMark * 10d) / 10d)
                .append(result).append("\r\n");
        return String.valueOf(stringBuilder);
    }

    List<Student> sortsListOfAverageMark(List<Student> studentsList) {
        studentsList.sort(Student.AverageMarkComparator);
        return studentsList;
    }

    List<Student> sortsListOfTimeUntilGraduation(List<Student> studentsList) {
        studentsList.sort(Student.StartDateReceiptOfTaskComparator);
        return studentsList;
    }

    List<Student> filtersListOfProbabilityTraining(List<Student> studentList) {
        List<Student> filteredList = new ArrayList<>();
        String nameOfStudents;
        WorkOnDatabaseOfStudents workOnDatabaseOfStudents = new WorkOnDatabaseOfStudents();
        for (Student student : studentList) {
            nameOfStudents = student.getLastName() + " " + student.getFirstName() + " " + student.getMiddleName();
            if (workOnDatabaseOfStudents.calculatePossibilityOfNotDeductingStudent(nameOfStudents)) {
                filteredList.add(student);
            }
        }
        return filteredList;
    }

    String printsStudentsList(List<Student> studentsList) {
        StringBuilder stringBuilder = new StringBuilder();
        for (Student student : studentsList) {
            stringBuilder = stringBuilder.append(printsStudent(student));
        }
        return String.valueOf(stringBuilder);
    }
}
