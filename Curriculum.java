package com.epam.training.javaBasics.stolbovy;

import java.util.List;

public class Curriculum {

    private String nameOfCurriculum;
    private List<Course> course;

    Curriculum(String nameOfCurriculum, List<Course> course) {
        this.nameOfCurriculum = nameOfCurriculum;
        this.course = course;
    }

    List<Course> getCourse() {
        return course;
    }

    String getNameOfCurriculum() {
        return nameOfCurriculum;
    }

    @Override
    public String toString() {
        return "Curriculum{" +
                "nameOfCurriculum='" + nameOfCurriculum + '\'' +
                ", course=" + course +
                '}';
    }
}