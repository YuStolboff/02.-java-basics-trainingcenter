package com.epam.training.javaBasics.stolbovy;

public class Course {


    private String nameOfCourse;
    private int duration;

    Course(String nameOfCourse, int duration) {
        this.nameOfCourse = nameOfCourse;
        this.duration = duration;
    }

    int getDuration() {
        return duration;
    }

    @Override
    public String toString() {
        return "Course{" +
                "nameOfCourse='" + nameOfCourse + '\'' +
                ", duration=" + duration +
                '}';
    }
}
