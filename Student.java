package com.epam.training.javaBasics.stolbovy;

import java.util.Comparator;
import java.util.List;

public class Student {

    private String lastName;
    private String firstName;
    private Curriculum curriculum;
    private int startDateReceiptOfTask;
    private String middleName;
    private List<Integer> mark;

    Student(String lastName, String firstName, Curriculum curriculum,
            int startDateReceiptOfTask, List<Integer> mark) {
        this(lastName, firstName, "", curriculum, startDateReceiptOfTask, mark);
    }

    Student(String lastName, String firstName, String middleName, Curriculum curriculum,
            int startDateReceiptOfTask, List<Integer> mark) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.curriculum = curriculum;
        this.startDateReceiptOfTask = startDateReceiptOfTask;
        this.middleName = middleName;
        this.mark = mark;
    }

    String getLastName() {
        return lastName;
    }


    String getFirstName() {
        return firstName;
    }

    Curriculum getCurriculum() {
        return curriculum;
    }

    int getStartDateReceiptOfTask() {
        return startDateReceiptOfTask;
    }

    String getMiddleName() {
        return middleName;
    }

    List<Integer> getMark() {
        return mark;
    }

    @Override
    public String toString() {
        return "Student{" +
                "lastName='" + lastName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", curriculum=" + curriculum +
                ", startDateReceiptOfTask=" + startDateReceiptOfTask +
                ", mark=" + mark +
                '}';
    }

    static Comparator<Student> StartDateReceiptOfTaskComparator = (o1, o2) -> {
        int dateReceiptOfTask = o1.getStartDateReceiptOfTask();
        int sumOfDuration = 0;
        int numberOfHoursUntilEndOfTrainingO1;
        int numberOfHoursUntilEndOfTrainingO2;
        Curriculum curriculum = o1.getCurriculum();
        for (Course course : curriculum.getCourse()) {
            sumOfDuration += course.getDuration();
        }
        numberOfHoursUntilEndOfTrainingO1 = sumOfDuration - dateReceiptOfTask;
        dateReceiptOfTask = o2.getStartDateReceiptOfTask();
        sumOfDuration = 0;
        curriculum = o2.getCurriculum();
        for (Course course : curriculum.getCourse()) {
            sumOfDuration += course.getDuration();
        }
        numberOfHoursUntilEndOfTrainingO2 = sumOfDuration - dateReceiptOfTask;
        return Integer.compare(numberOfHoursUntilEndOfTrainingO1, numberOfHoursUntilEndOfTrainingO2);
    };


    static Comparator<Student> AverageMarkComparator = (o1, o2) -> {
        int numberOfMarks = 0;
        double sumOfMarks = 0;
        for (Integer mark : o1.getMark()) {
            sumOfMarks += mark;
            numberOfMarks++;
        }
        double averageMarkO1 = sumOfMarks / numberOfMarks;
        numberOfMarks = 0;
        sumOfMarks = 0;
        for (Integer mark : o2.getMark()) {
            sumOfMarks += mark;
            numberOfMarks++;
        }
        double averageMarkO2 = sumOfMarks / numberOfMarks;
        return Double.compare(averageMarkO1, averageMarkO2);
    };
}
