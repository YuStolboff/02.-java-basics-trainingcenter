package com.epam.training.javaBasics.stolbovy;

import java.util.ArrayList;
import java.util.List;

class Database {

    private final static int NUMBER_OF_HOURS = 8;
    private static List<Course> coursesOfJavaSE = coursesDataOfJavaSE();
    private static List<Course> coursesOfJavaEE = coursesDataOfJavaEE();
    static List<Student> students = studentsData();

    private static List<Course> coursesDataOfJavaSE() {
        List<Course> coursesOfJavaSE = new ArrayList<>();
        coursesOfJavaSE.add(0, new Course("JDBC", new ValueGenerator().generatesNaturalNumber("duration")));
        coursesOfJavaSE.add(1, new Course("IO", new ValueGenerator().generatesNaturalNumber("duration")));
        coursesOfJavaSE.add(2, new Course("JFC", new ValueGenerator().generatesNaturalNumber("duration")));
        coursesOfJavaSE.add(3, new Course("JAX", new ValueGenerator().generatesNaturalNumber("duration")));
        coursesOfJavaSE.add(4, new Course("JSON", new ValueGenerator().generatesNaturalNumber("duration")));
        coursesOfJavaSE.add(5, new Course("XML", new ValueGenerator().generatesNaturalNumber("duration")));
        coursesOfJavaSE.add(6, new Course("NIO", new ValueGenerator().generatesNaturalNumber("duration")));
        return coursesOfJavaSE;
    }

    private static List<Course> coursesDataOfJavaEE() {
        List<Course> coursesOfJavaEE = new ArrayList<>();
        coursesOfJavaEE.add(0, new Course("Spring", new ValueGenerator().generatesNaturalNumber("duration")));
        coursesOfJavaEE.add(1, new Course("Struts", new ValueGenerator().generatesNaturalNumber("duration")));
        coursesOfJavaEE.add(2, new Course("Hibernate", new ValueGenerator().generatesNaturalNumber("duration")));
        coursesOfJavaEE.add(3, new Course("Servlets", new ValueGenerator().generatesNaturalNumber("duration")));
        coursesOfJavaEE.add(4, new Course("Maven", new ValueGenerator().generatesNaturalNumber("duration")));
        coursesOfJavaEE.add(5, new Course("Java Performance", new ValueGenerator().generatesNaturalNumber("duration")));
        coursesOfJavaEE.add(6, new Course("Netty", new ValueGenerator().generatesNaturalNumber("duration")));
        return coursesOfJavaEE;
    }

    static private Curriculum generatesCurriculum() {
        String nameOfCurriculum = new ValueGenerator().generatesNameOfCurriculum();
        return new Curriculum(nameOfCurriculum,
                nameOfCurriculum.equals("J2EE Developer") ? coursesOfJavaEE : coursesOfJavaSE);
    }

    private static List<Student> studentsData() {
        List<Student> students = new ArrayList<>();
        int startDateReceiptOfTask = new ValueGenerator().generatesNaturalNumber("startDateReceiptOfTask");
        Curriculum curriculum = generatesCurriculum();
        students.add(0, new Student("Ivanov", "Ivan", "Ivanovich", curriculum, startDateReceiptOfTask,
                generatesMarks(startDateReceiptOfTask, curriculum.getNameOfCurriculum())));
        startDateReceiptOfTask = new ValueGenerator().generatesNaturalNumber("startDateReceiptOfTask");
        curriculum = generatesCurriculum();
        students.add(1, new Student("Petrov", "Ivan", "Ivanovich", curriculum, startDateReceiptOfTask,
                generatesMarks(startDateReceiptOfTask, curriculum.getNameOfCurriculum())));
        startDateReceiptOfTask = new ValueGenerator().generatesNaturalNumber("startDateReceiptOfTask");
        curriculum = generatesCurriculum();
        students.add(2, new Student("Ivanov", "Petr", "Ivanovich", curriculum, startDateReceiptOfTask,
                generatesMarks(startDateReceiptOfTask, curriculum.getNameOfCurriculum())));
        startDateReceiptOfTask = new ValueGenerator().generatesNaturalNumber("startDateReceiptOfTask");
        curriculum = generatesCurriculum();
        students.add(3, new Student("Sidorov", "Ivan", curriculum, startDateReceiptOfTask,
                generatesMarks(startDateReceiptOfTask, curriculum.getNameOfCurriculum())));
        startDateReceiptOfTask = new ValueGenerator().generatesNaturalNumber("startDateReceiptOfTask");
        curriculum = generatesCurriculum();
        students.add(4, new Student("Sidorov", "Sidor", "Ivanovich", curriculum, startDateReceiptOfTask,
                generatesMarks(startDateReceiptOfTask, curriculum.getNameOfCurriculum())));
        startDateReceiptOfTask = new ValueGenerator().generatesNaturalNumber("startDateReceiptOfTask");
        curriculum = generatesCurriculum();
        students.add(5, new Student("Sergeev", "Ivan", "Ivanovich", curriculum, startDateReceiptOfTask,
                generatesMarks(startDateReceiptOfTask, curriculum.getNameOfCurriculum())));
        startDateReceiptOfTask = new ValueGenerator().generatesNaturalNumber("startDateReceiptOfTask");
        curriculum = generatesCurriculum();
        students.add(6, new Student("Ivanov", "Sergey", "Ivanovich", curriculum, startDateReceiptOfTask,
                generatesMarks(startDateReceiptOfTask, curriculum.getNameOfCurriculum())));
        startDateReceiptOfTask = new ValueGenerator().generatesNaturalNumber("startDateReceiptOfTask");
        curriculum = generatesCurriculum();
        students.add(7, new Student("Ivanov", "Ivan", "Sergeevich", curriculum, startDateReceiptOfTask,
                generatesMarks(startDateReceiptOfTask, curriculum.getNameOfCurriculum())));
        startDateReceiptOfTask = new ValueGenerator().generatesNaturalNumber("startDateReceiptOfTask");
        curriculum = generatesCurriculum();
        students.add(8, new Student("Petrov", "Sergey", "Ivanovich", curriculum, startDateReceiptOfTask,
                generatesMarks(startDateReceiptOfTask, curriculum.getNameOfCurriculum())));
        startDateReceiptOfTask = new ValueGenerator().generatesNaturalNumber("startDateReceiptOfTask");
        curriculum = generatesCurriculum();
        students.add(9, new Student("Sidorov", "Sergey", "Ivanovich", curriculum, startDateReceiptOfTask,
                generatesMarks(startDateReceiptOfTask, curriculum.getNameOfCurriculum())));
        startDateReceiptOfTask = new ValueGenerator().generatesNaturalNumber("startDateReceiptOfTask");
        curriculum = generatesCurriculum();
        students.add(10, new Student("Sergeev", "Petr", "Ivanovich", curriculum, startDateReceiptOfTask,
                generatesMarks(startDateReceiptOfTask, curriculum.getNameOfCurriculum())));
        startDateReceiptOfTask = new ValueGenerator().generatesNaturalNumber("startDateReceiptOfTask");
        curriculum = generatesCurriculum();
        students.add(11, new Student("Sergeev", "Petr", "Sergeevich", curriculum, startDateReceiptOfTask,
                generatesMarks(startDateReceiptOfTask, curriculum.getNameOfCurriculum())));
        startDateReceiptOfTask = new ValueGenerator().generatesNaturalNumber("startDateReceiptOfTask");
        curriculum = generatesCurriculum();
        students.add(12, new Student("Sidorov", "Petr", "Ivanovich", curriculum, startDateReceiptOfTask,
                generatesMarks(startDateReceiptOfTask, curriculum.getNameOfCurriculum())));
        startDateReceiptOfTask = new ValueGenerator().generatesNaturalNumber("startDateReceiptOfTask");
        curriculum = generatesCurriculum();
        students.add(13, new Student("Sergeev", "Sidor", "Ivanovich", curriculum, startDateReceiptOfTask,
                generatesMarks(startDateReceiptOfTask, curriculum.getNameOfCurriculum())));
        startDateReceiptOfTask = new ValueGenerator().generatesNaturalNumber("startDateReceiptOfTask");
        curriculum = generatesCurriculum();
        students.add(14, new Student("Sidorov", "Sergey", "Sergeevich", curriculum, startDateReceiptOfTask,
                generatesMarks(startDateReceiptOfTask, curriculum.getNameOfCurriculum())));
        startDateReceiptOfTask = new ValueGenerator().generatesNaturalNumber("startDateReceiptOfTask");
        curriculum = generatesCurriculum();
        students.add(15, new Student("Petrov", "Sidor", "Petrovich", curriculum, startDateReceiptOfTask,
                generatesMarks(startDateReceiptOfTask, curriculum.getNameOfCurriculum())));
        return students;
    }

    private static List<Integer> generatesMarks(int startDateReceiptOfTask, String nameOfCurriculum) {
        List<Integer> marks = new ArrayList<>();
        int sumDurations = 0;
        for (Course course : nameOfCurriculum.equals("J2EE Developer") ? coursesOfJavaEE :
                coursesOfJavaSE) {
            sumDurations += course.getDuration();
        }
        if (sumDurations > startDateReceiptOfTask) {
            for (int i = 0; i < startDateReceiptOfTask / NUMBER_OF_HOURS; i++) {
                marks.add(new ValueGenerator().generatesNaturalNumber("mark"));
            }
        } else {
            for (int i = 0; i < sumDurations; i++) {
                marks.add(new ValueGenerator().generatesNaturalNumber("mark"));
            }
        }
        return marks;
    }
}
